# Welcome to Ghoul!
Ghoul is a reimplementation of the [Ghost blogging platform's](https://ghost.org/)
publishing dashboard and editing flow. The intention for Ghoul is to provide a
clean, polished frontend for publishing, while relying on Pleroma as a backend.

## Current Status
Nothing works yet, and only the most basic skeleton is present.



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
